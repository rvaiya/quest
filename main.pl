#!/usr/bin/env perl

use strict;
use utf8;

use URI::Escape;
use LWP::UserAgent;
use WWW::Mechanize;
use Data::Dumper;
use Clone 'clone';
use HTML::TreeBuilder::XPath;

#TODO benchmark and optimize

my $mech = WWW::Mechanize->new;

sub getmech {
    my ($user, $pass) = @_;
    my $ua = WWW::Mechanize->new;
    $ua->agent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36');
    $ua->cookie_jar({ignore_discard => 1});
    $ua->cookie_jar()->load("$ENV{'HOME'}/quest_cookies.dat");

    die "Failed to login" unless(login($user, $pass, $ua));

    $ua->cookie_jar()->save("$ENV{'HOME'}/quest_cookies.dat");
    return $ua;
}

sub login {
    my ($user, $pass, $ua) = @_;
    return 1 if($ua->get('https://quest.pecs.uwaterloo.ca/psp/SS/ACADEMIC/SA/c/SA_LEARNER_SERVICES.SSS_STUDENT_CENTER.GBL?PORTALPARAM_PTCNAV=HC_SSS_STUDENT_CENTER&EOPP.SCNode=SA&EOPP.SCPortal=ACADEMIC&EOPP.SCName=CO_EMPLOYEE_SELF_SERVICE&EOPP.SCLabel=Self%20Service&EOPP.SCPTfname=CO_EMPLOYEE_SELF_SERVICE&FolderPath=PORTAL_ROOT_OBJECT.CO_EMPLOYEE_SELF_SERVICE.HC_SSS_STUDENT_CENTER&IsFolder=false')->decoded_content =~ /Student Center/); #Don't login if already logged in
    print "Valid session not found, attempting to log in...\n";

    $ua->cookie_jar({}) unless $ua->cookie_jar();
    $ua->post('https://quest.pecs.uwaterloo.ca/psp/SS/?cmd=login&languageCd=ENG', { userid => $user, pwd => $pass });
    

    my $success = 0;
    $ua->cookie_jar()->scan(sub {$success++ if($_[1] eq "PS_TOKEN");});

    return $success;
}

#Place mech on a search results page corresponding to the given subject and number
sub search {
    my ($mech, $subject, $nbr) = @_;

    $mech->get('https://quest.pecs.uwaterloo.ca/psc/SS/ACADEMIC/SA/c/SA_LEARNER_SERVICES.SSR_SSENRL_LIST.GBL?Page=SSR_SSENRL_LIST&Action=A');
    follow_link($mech, text => 'search for classes');

    $mech->form_name('win0');
    $mech->set_fields('CLASS_SRCH_WRK2_SUBJECT$7$' => $subject, 'CLASS_SRCH_WRK2_CATALOG_NBR$8$' => $nbr, 'CLASS_SRCH_WRK2_SSR_OPEN_ONLY$chk' => "N");

    follow_link($mech, id => 'UW_DERIVED_SR_SSR_PB_CLASS_SRCH');
}

sub follow_link {
    #Custom link click for Mechanize which which can handle certain javascript links
    my $mech = shift @_;

    my $link = $mech->find_link(@_);
    die("Could not find links using params: " . Dumper @_) unless $link;

    $link->url() =~ /^javascript:submitAction_win0\(document.win0,\s*'(.*)'\s*\)/;
    my $action = $1;

    if($action) { 
	perform_ics_action($mech, $action);
    } else {
	die("Could not handle link", $link->url) unless $link->url_abs =~ /http/;
	$mech->get($link->url_abs());
    }
}

sub perform_ics_action {
    #Most links/buttons calls submitAction_win0 which makes small alterations to the form and then submits it,
    #the most crucial is ICAction which needs to be set for the backend to properly process the request.

    my ($mech, $action) = @_;
    $mech->submit_form(
	form_name => 'win0',
	fields => { ICAction => $action });
}

#Given a class/course table parse out section information
sub parse_section_table {
    my ($section_table) = @_;

    my @rows = $section_table->content_list();
    
    #Delete 'combined section' rows since they violate assumed component spacing (7 rows)
    @rows = grep { !$_->findnodes('.//*[starts-with(@name, "UW_DERIVED_SR_CMB_SCT_DTL_PB")]') } @rows; 
    
    my @result = ();
    
    if(@rows == 7) {
	push @result, parse_component(@rows);
    } elsif((@rows + 1) % 7 != 0) { #Total number of rows should be divisible by 7
	die "Failed to parse class table ", $section_table->as_HTML();
    } else {
	push @rows, 'junk'; #Last component only consists of 6 rows
	while(my @component = splice @rows, 0, 7) {
	    push @result, parse_component(@component);
	}
    }
    
    return @result;
}

#Consumes the seven rows constituting a component entry and parses them
sub parse_component {
    my ($section, $status, $session_type, $schedule) = @_[1,2,4,5];
    my @schedule =();
    
    $section = (($section->content_list())[1])->as_text;
    $status = $status->look_down(_tag => 'img')->attr('alt');
    $session_type = $session_type->as_text();
    
    my $result = {
	section => $section,
	status => $status,
	session_type => $session_type,
    };
    
    my @sched_entries = $schedule->look_down(class => 'PSLEVEL1GRIDWBO')->content_list();
    shift @sched_entries; #Strip headers
    for (@sched_entries) {
	my @r = $_->content_list();
	my ($time, $room, $instructor, $dates) = map { $_->as_text(); } @r;
	
	$time =~ s/[^a-zA-Z0-9\/:-]+/ /g;
	$instructor =~ s/[^a-zA-Z0-9\/:-]+/ /g;
	$room =~ s/[^a-zA-Z0-9\/:-]+/ /g;
	$dates =~ s/[^a-zA-Z0-9\/:-]+/ /g;
	push @schedule, {
	    time => $time,
	    instructor => $instructor,
	    room => $room,
	    dates => $dates
	};
    }
    
    $result->{schedule} = \@schedule;
    return $result;
}

#Parse search results when provided with the root node of a results page.
sub parse_search_results {
    my ($root) = @_;
    my ($class_table) = $root->findnodes('//table[starts-with(@id, "ACE_$ICField102")]') or die "No results found";
    
    
    my @class_table_entries = $class_table->content_list();
    my @courses = ();
    
    while(@class_table_entries) {
	my (undef, $title, undef, $section_table) = splice @class_table_entries, 0, 4;
	($section_table) = $section_table->findnodes('.//table[starts-with(@id, "ACE_$ICField106")]');
	
	$title = $title->as_text();
	$title =~ s/[^a-zA-Z0-9\/:-]+/ /g;
	
	my $sep = index($title, "-");
	my $course = substr $title, 0, $sep - 1;
	$title = substr $title, $sep + 2;
	
	my @sections = parse_section_table($section_table);
	
	push @courses, {
	    course => $course,
	    title => $title,
	    sections => \@sections
	};
    }

    return @courses;
}

#Main

#Performs a diff on two class lists and returns the newly opened sections
#The status returned is value from the second list.


sub diff_open_sections {
    my @list1 = @{shift()};
    my @list2 = @{shift()};
    
    my @changed = ();
    my %index1 = ();
    
    print "Generating index over list1...\n";
    for my $class (@list1) {
	for(@{$class->{sections}}) {
	    $index1{$class->{course} . $_->{section}} = $_->{status};
	}
    }

    for my $class (@list2) {
	for (@{$class->{sections}}) {
	    if($index1{$class->{course} . $_->{section}} ne $_->{status}) {
		push @changed, {
		    course => $class->{course},
		    section => $_->{section},
		    newstatus => $_->{status}
		};
	    }
	}
    }

    return @changed;
}

my @class_list = ();
while(1) {
    my $mech = getmech($ENV{QUEST_USER}, $ENV{QUEST_PASSWORD});
    search($mech, 'cs');
    perform_ics_action($mech, '#ICSave');
    my $root = HTML::TreeBuilder::XPath->new_from_content($mech->content);
    my @temp = parse_search_results($root);

    if(@class_list) {
	my @diff = diff_open_sections(@class_list, @temp);
	print Dumper \@diff if(@diff);
    }
    
    @class_list = @temp;
    sleep 10;
}
